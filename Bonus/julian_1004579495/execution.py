import numpy as np
import matplotlib.pyplot as plt
from tiro import MovimientoProyectil

if __name__ == '__main__':
    
    projectile = MovimientoProyectil(angulo=30, velocidad_inicial=10, altura=10)
    time_of_flight = projectile.calculate_time_of_flight()
    

    volando=np.arange(0,time_of_flight,0.01)
    x_gra=[]
    y_gra=[]
    
    tiempo_vuelo=f"Tiempo de Vuelo: {np.round(time_of_flight,2)} s"
    print(tiempo_vuelo)
    

    # Calculate and print position at different times
    for i in volando:
        position = projectile.calculate_position(i)
        x_gra.append(position[0])
        y_gra.append(position[1])
        

    plt.plot(x_gra,y_gra)    
    plt.suptitle('Movimiento parabolico')
    plt.title(tiempo_vuelo)
        
    plt.ylabel('$y(t)$')
    plt.xlabel('$x(t)$')
    plt.show()
    