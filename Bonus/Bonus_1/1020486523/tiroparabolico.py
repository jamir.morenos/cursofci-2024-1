import numpy as np
import matplotlib.pyplot as plt 
class tiroParabolico:
    
    """Definición de los parametros para hallar la velocidad en x en y y el tiempo de vuelo
    theta: representa el angulo de la velocidad inicial imprimida al objeto
    v0: velocidad inicial con la que fue lanzado el objeto, en grados
    x0: posicion inicial
    h0: altura inicial 
    """
    def __init__(self,theta,v0,x0,h0,g):
        self.theta=theta
        self.v0=v0
        self.x0=x0
        self.h0=h0
        self.g=g

    def velx(self):
        return self.v0*np.cos(self.theta)
    
    def tfly(self):
        return 2*self.v0*np.sin(self.theta)/self.g
    
    def vely(self):
        return self.v0*np.sin(self.theta)-self.g*np.linspace(0,self.tfly(),100)
    
    def tfly(self):
        return (self.v0*np.sin(self.theta)+np.sqrt(( self.v0*np.sin(self.theta))**2 +2*self.g*self.h0))/self.g

    
    def xpos(self):
        return self.x0+self.velx()*np.linspace(0,self.tfly(),100)
    
    def ypos(self):
        return self.h0+self.v0*np.sin(self.theta)*(np.linspace(0,self.tfly(),100))-0.5*self.g*(np.linspace(0,self.tfly(),100))**2
    
    def grafica(self):
        plt.plot(self.xpos(),self.ypos(),'o')
        plt.xlabel("x(m)")
        plt.ylabel("y(m)")
        plt.legend()
        plt.show()


class tiroParabolico2(tiroParabolico):
    
    def __init__(self, theta, v0, x0, h0, g,acelx):
        super().__init__(theta, v0, x0, h0, g)

        self.ax=acelx
    
    def velx(self):
        return self.v0*np.cos(self.theta)+self.ax*np.linspace(0,self.tfly(),100)

    def xpos(self):
        return self.x0+self.velx()*np.linspace(0,self.tfly(),100)+0.5*self.ax*(np.linspace(0,self.tfly(),100))**2

    