# Andrés Felipe Riaño Quintanilla 1083928808
# Santiago Julio Dávila 1000413445

if __name__ == '__main__':
    import numpy as np
    import matplotlib.pyplot as plt
    from classes_RK import RKG

    # Parámetros para el RK4
    ord = 4
    cc = [0,1/2,1/2,1]
    aa = [[1/2],[0,1/2],[0,0,1]]
    bb = [1/6,1/3,1/3,1/6]
    h = 1e-5

    # Función que representa la derivada
    f = lambda x,y: x-y

    # Condiciones iniciales y finales
    ini = [0.,0.]
    xf = 1.

    # Solución exacta
    ex = lambda x:x-1+1*np.exp(-x)

    # Instancia de la clase
    RK = RKG(ord,cc,aa,bb,h,f,ini,xf)
    RK.soluciones('y',exact=ex)
    RK.convergencia(ex)