import matplotlib.pyplot as plt
import numpy as np


class RKG():
    """
    Runge-Kutta explícito generalizado.

    Atributos
    --------
    t0 : float
        punto inicial de la variable independiente
    y0 : float
        condición inicial y0=y(t0)
    tf : float
        punto final del cálculo 
    h : float
        tamaño del paso
    f : function
        derivada de y, y' = f(t,y)
    order : int
        orden del método RK
    A : matrix
        matriz de Runge-Kutta
    b : list
        coeficiente de los pesos
    c : list
        coeficiente de los nodos
    t : array
        array de la variable independiente, desde t0 hasta tf con un paso de h
    y : array
        array con las soluciones
    
    Métodos
    -------
    ks : array
        retorna un array con los ks en un punto (t,y)
    sol : array
        retorna las soluciones en cada punto tn
    convergence : None
        crea una gráfica de la solución y su convergencia en el directorio
    """
    def __init__(self,f,t0: float,y0: float,tf: float,h: float,order =4,A:list = None,b:list = None,c:list = None) -> None:
        """
        Parámetros
        ---------
        f : function
            derivada de y, y' = f(t,y).
        t0 : float
            valor inicial de la variable independiente t.
        y0 : float
            valor inicial y.
        tf : float
            valor hasta el que se calcula la solución.
        h : float
            tamaño del paso
        order : int, opcional
            orden del Runge-Kutta (defecto es 4)

        A,b,c : list, opcional
            Matriz de Runge-Kutta,vector de pesos y nodos del RGK explícito generalizad

            si no son ingresados y el orden es 4, usa los coeficientes del RK4 por defecto.
        """
        self.t0 = t0 
        self.y0 = y0
        self.tf = tf
        self.h = h
        self.f = f
        self.order = order

        #un poco de manejo de errores para mejor experiencia y determinación de los coeficientes por defecto (RK4)
        if self.order == 4 and all(x is None for x in (A,b,c)):
            self.A =  [[0,0,0,0],
                       [1/2,0,0,0],
                       [0,1/2,0,0],
                       [0,0,1,0]]

            self.b = [1/6,1/3,1/3,1/6]

            self.c = [0,1/2,1/2,1]
        else: #si el orden no es 4 o se ingresa manualmente un coeficiente, se debe ingresar todos los demás.
            if any(x is None for x in (A,b,c)):
                raise Exception("Debe ingresar todos los coeficientes")
            else:
                self.A = A
                self.b = b
                self.c = c

        if any(x!=self.order for x in (len(self.A),len(self.b),len(self.c))):#chequea que las dimensiones de los coeficientes cuadren
            raise Exception(f"La dimensión de los coeficientes debe ser {self.order}")

        #TODO: añadir una advertencia para coeficientes inconsistentes, por ejemplo si la suma de las componentes de b no es 1
        
    @property
    def t(self):
        return np.arange(self.t0,self.tf+self.h,self.h)
    
    def sol(self):
        """
        Retorna las soluciones de la EDO en cada punto t
        """
        y = [self.y0]
        for t in self.t[:-1]:
            y += [y[-1]+self.h*np.dot(self.b,self.ks(t,y[-1]))]

        return np.array(y)


    def ks(self,tn,yn):
        """
        Retorna un array con los coeficientes k para el punto (tn,yn)

        Parámetros
        ---------
        tn : float
            punto t
        yn : float
            punto y
        """

        ks = np.zeros(self.order)
        for stage in range(len(ks)):
            ks[stage] = self.f(tn+self.c[stage]*self.h,yn+self.h*np.dot(self.A[stage][:stage],ks[:stage]))
        return ks

    def convergence(self,npoints: list,exactF):
        """
        Entrega gráficas de convergencia en el directorio
        
        Parámetros
        ----------
        npoints: list
            número de puntos para analizar la convergencia
        exactF: function
            solución exacta
        """
        npoints = np.array(npoints)

        h0 = self.h #original h
        hs=(self.tf-self.t0)/npoints
        ydif = []

        fig, (ax1,ax2)=plt.subplots(1,2,figsize=(13,7),dpi=150)

        ax1.plot(self.t,self.sol(),label = 'Solución numérica')
        ax1.plot(self.t,exactF(self.t),'k--', label= 'Solución exacta')
        ax1.legend()

        ax1.set_title(f"Solución de la EDO para un paso de {self.h}",fontsize=15)
        ax1.set_xlabel("x",fontsize=15)
        ax1.set_ylabel("y",fontsize=15)
        #------------------------
        #Gráfica de convergencia
        #-----------------------
        for h in hs:
            self.h = h
            ydif.append(np.mean(np.abs(self.sol()-exactF(self.t))))

        self.h = h0

        ax2.plot(hs,ydif)
        ax2.set_title("Convergencia de la solución")
        ax2.set_xlabel("h", fontsize=15)
        ax2.set_ylabel("$\Delta y$",fontsize=15)

        plt.savefig('punto2.png')

class RKG_A(RKG):
    """
    Vectorización de la clase RKG
    """
    def __init__(self, f, t0: float, y0: float, tf: float, h: float, order=4, A: list = None, b: list = None, c: list = None) -> None:
        super().__init__(f, t0, y0, tf, h, order, A, b, c)
    
    def ks(self,tn,yn):
        """
        Retorna un array multidimensional con los coeficientes k para el punto (tn,yn)

        Parámetros
        ---------
        tn : float
            punto t
        yn : array
            punto y
        """
        ks = np.zeros((len(yn),self.order))

        for stage in range(self.order):
            ks[:,stage] = self.f(tn+self.c[stage]*self.h,yn+self.h*np.array([np.dot(self.A[stage][:stage],ks[i,:stage]) for i in range(len(yn))]))
        return ks

    def sol(self):
        """
        Retorna las soluciones de la EDO vectorizada
        """
        y = [self.y0]
        for t in self.t[:-1]:
            y += [y[-1]+self.h*np.array(self.ks(t,y[-1])@self.b)]

        return np.array(y)
    



if __name__ == "__main__":
    def exactF(x):
        return x-1+1/np.exp(x)

    initialCon = [0,0] #condiciones iniciales x0,y0
    xf = 1.
    h = 0.00001
    sol = RKG(lambda x,y:x-y,*initialCon,xf,h)
    sol.convergence([10,100,1000,10000],exactF)

    

    
    